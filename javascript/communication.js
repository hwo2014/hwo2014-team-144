var net = require("net");
var JSONStream = require('JSONStream');
var games = require( './game' );

var communication = {

  serverPort: 8091,
  serverHost: 'testserver.helloworldopen.com',
  botName: 'ryan',
  botKey: 'c7dTGj3xYFKYbg',
  carCount: 1,
  trackName: '',

  client: null,
  jsonStream: null,

  // will hold all currently running games
  games: [],

  // create all other objects we need
  init: function( ) {
    console.log( 'Bot: ' + this.botName );
    console.log( 'Server: ' + this.serverHost + ':' + this.serverPort);
    // init client
    this.initClient( );
  },

  // initializes the client
  initClient: function( ) {
    var that = this;
    this.client = net.connect(this.serverPort, this.serverHost, function() {
      // init a game
      that.initGame( );
    });

    this.jsonStream = this.client.pipe(JSONStream.parse());

    this.jsonStream.on('data', function(data) {
      // main object acts on communication
      that.act( data );
    });

    this.jsonStream.on('error', function() {
      return console.log("disconnected");
    });
  },

  // initializes a first game
  initGame: function( ) {
    // init an initial game
    var game = this.createGame( );

    this.games.push( game );
  },

  quit: function( msg ) {
    console.log( msg );
    console.log( 'HALTING' );
    process.exit( );
  },

  act: function( req ) {
    //console.log( 'CURRENT: '+JSON.stringify( req ) );
    switch( req.msgType ) {
      // a success for the bot to a game init
      case 'gameInit':
        var game = this.getGame( req.gameId );
        // first check wether we already have a game
        //if( !game || game.isInit ) this.quit( 'ERROR: Tried to reinit known game' );
        game.receiveInit( req );
        break;

      // success for a join game operation
      case 'join':
        // now we get a join WITHOUT a gameId, so we ASSUME we only have ONE game, if NOT, there must be an error
        if( !this.games[ 0 ].isJoined ) this.games[ 0 ].receiveJoin( req );
        break;

      case 'createRace':
        if( !this.games[ 0 ].isJoined ) this.games[ 0 ].receiveJoin( req );
        break;

      case 'joinRace':
        if( !this.games[ 0 ].isJoined ) this.games[ 0 ].receiveJoin( req );
        break;

      // information about your car
      case 'yourCar':
        var game = this.getGame( req.gameId );
        if( game == null ) {
          // now if we only have one game, just use this one
          if( this.games.length == 1 ) {
            game = this.games[ 0 ];
            // also set the gameId now too
            game.gameId = req.gameId;
          }
          else this.quit( 'ERROR: No game found by gameId')
        }
        game.receiveCarInfo( req );
        // Todo: 'https://hwo2014-racedata-prod.s3.amazonaws.com/test-races/' + req.gameId sind die gesammelten Daten des
        //       Rennens und 'https://helloworldopen.com/race-visualizer/?v=1398189562866&recording=' + obige URL ist der
        //       Visualisierer für das Rennen
        console.log( 'Game-ID: ' + req.gameId );
        break;

      case 'carPositions':
        var game = this.getGame( req.gameId );
        if( !game ) this.quit( 'ERROR: Something wrong with a running game not being properly initialized' );
        game.receiveCarPositions( req );
        break;

      case 'gameStart':
        var game = this.getGame( req.gameId );
        if( !game ) this.quit( 'ERROR: What really? No game here? This is ridiculous!' );
        game.receiveGameStart( req );
        break;

      case 'lapFinished':
        var game = this.getGame( req.gameId );
        if( !game ) this.quit( 'ERROR: What really? No game here? This is beyond insane!' );
        game.receiveLapInfo( req );
        break;

      case 'crash':
        var game = this.getGame( req.gameId );
        if( !game ) this.quit( 'ERROR: What really? No game here? This is universally stupid!' );
        game.receiveCrash( req );
        break;

      case 'spawn':
        var game = this.getGame( req.gameId );
        if( !game ) this.quit( 'ERROR: What really? No game here? This is outrageous!' );
        game.receiveSpawn( req );
        break;

      case 'finish':
        var game = this.getGame( req.gameId );
        if( !game ) this.quit( 'ERROR: What really? No game here? This is inappropriate!' );
        game.receiveFinish( req );
        break;

      case 'turboAvailable':
        var game = this.getGame( req.gameId );
        if( !game ) this.quit( 'ERROR: What really? No game here? This is catatonic!' );
        game.receiveTurboAvailable( req );
        break;

      case 'turboStart':
        var game = this.getGame( req.gameId );
        if( !game ) this.quit( 'ERROR: What really? No game here? This is terrible!' );
        game.receiveTurboStart( req );
        break;

      case 'turboEnd':
        var game = this.getGame( req.gameId );
        if( !game ) this.quit( 'ERROR: What really? No game here? This is bad!' );
        game.receiveTurboEnd( req );
        break;

      case 'gameEnd':
        var game = this.getGame( req.gameId );
        if( !game ) this.quit( 'ERROR: What really? No game here? This is stupid!' );
        game.receiveGameEnd( req );
        break;

      case 'tournamentEnd':
        var game = this.getGame( req.gameId );
        if( !game ) this.quit( 'ERROR: What really? No game here? This is obnoxious!' );
        game.receiveTournamentEnd( req );
        break;

      case 'error':
        console.log( 'ERROR: '+JSON.stringify( req ) );
        break;

      case 'dnf':
        var game = this.getGame( req.gameId );
        if( !game ) this.quit( 'ERROR: What really? No game here? This is grotesque!' );
        console.log( 'ERROR: Got disqualified because: '+req.data.reason );
        break;

      default:
        //this.quit( 'WRN: Unknown: '+JSON.stringify( req ) );
        console.log( 'WRN: Unknown: '+JSON.stringify( req ) );
        break;
    }
  },

  // checks wether there already exists a game with the given gameId
  hasGame: function( gameId ) {
    return ( this.getGame( gameId ) != null );
  },

  // returns a game by the given gameId
  getGame: function( gameId ) {
    for( var i = 0; i < this.games.length; i++ ) {
      var game = this.games[ i ];
      if( game.gameId == gameId ) return game;
    }
    return null;
  },

  createGame: function( ) {
    //console.log( 'LOG: Creating new game' );
    var game = Object.create( games.game );
    game.com = this;
    game.name = this.botName;
    game.key = this.botKey;
    game.carCount = this.carCount;
    game.trackName = this.trackName;
    this.send( game.init( ) );
    return game;
  },

  send: function (json) {
    this.client.write(JSON.stringify(json));
    return this.client.write('\n');
  }
};

// call constructor

exports.communication = communication;
