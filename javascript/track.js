var track = {
  id: '',
  name: '',

  pieces: null,
  lanes: null,

  startingPoint: null,

  //
  //
  //
  init: function( data ) {
    this.id = data.id;
    this.name = data.name;

    this.pieces = data.pieces;
    this.lanes = data.lanes;

    this.startingPoint = data.startingPoint;

    this.data = data;
  },

  //
  //
  //
  getPiece: function( id ) {

    return this.pieces[ id ];

  },

  //
  //
  //
  pieceLength: function( piece, lane ) {

    if( piece.angle < 0 ) {
      return -(piece.radius + this.lanes[ lane ].distanceFromCenter) * Math.PI * piece.angle / 180;
    } else if( piece.angle > 0 ) {
      return (piece.radius - this.lanes[ lane ].distanceFromCenter) * Math.PI * piece.angle / 180;
    } else {
      return piece.length;
    }

  }

};

exports.track = track;