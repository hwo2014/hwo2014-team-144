var com = require( './communication' );

// Kommandozeilenparameter annehmen
for( var i = 2; i < process.argv.length; i++ ) {
  var data = process.argv[ i ].split( '=' );
  var name = data.shift( );
  var value = data.join( '=' );
  if( name + '=' + value !== process.argv[ i ] ) { // Keine "modernen" Parameter?
    com.communication.serverHost = process.argv[ 2 ];
    com.communication.serverPort = process.argv[ 3 ];
    com.communication.botName = process.argv[ 4 ];
    com.communication.botKey = process.argv[ 5 ];
    break;
  }
  switch( name ) {
    case '--botKey': com.communication.botKey = value; break;
    case '--botName': com.communication.botName = value; break;
    case '--carCount': com.communication.carCount = new Number( value ); break;
    case '--trackName': com.communication.trackName = value; break;
    case '--serverHost': com.communication.serverHost = value; break;
    case '--serverPort': com.communication.serverPort = value; break;
  }
}

com.communication.init( );
