var car = {
  name: '',
  color: '',
  dimensions: null,

  isCrashed: false,
  currentThrottle: 0.65,
  currentAngle: 0,
  currentPiecePosition: { },
  oldPiece: -1,
  raceDistance: 0,
  raceDistanceOffset: 0,
  pieceLength: 0,
//  driftAngle: 0,
//  driftEnergy: 0,
  curve1: 0.53033005,
  curve2: 0.3,
  throttle: 0,
  friction: 0.98,
  power: 0.2,
  lapOffset: 0,
  loggedData: [ ],
  speedHistory: [ ],

  hasTurbo: false,

  // the current game object
  game: null,

  //
  //
  //
  init: function( data ) {

    this.name = data.name;
    this.color = data.color;

  },

  //
  //
  //
  updateCarDimensions: function( data ) {

    this.dimensions = data;

  },

  //
  //
  //
  updateCarPosition: function( data ) {

    this.currentAngle = data.angle;
    this.currentPiecePosition = data.piecePosition;

  },

  //
  // THIS is the main logic
  //
  sendMessage: function( ) {

    if( !this.game.isStarted ) return { msgType: 'ping' };

    var switchTo = false;

    // Sind wir auf einem neuen Abschnitt der Rennstrecke?
    if( this.oldPiece !== this.currentPiecePosition.pieceIndex ) {
      this.raceDistanceOffset += this.pieceLength;
      this.oldPiece = this.currentPiecePosition.pieceIndex;
      if( this.game.track.pieces[ (this.currentPiecePosition.pieceIndex + 1) % this.game.track.pieces.length ].switchTo ) {
        switchTo = this.game.track.pieces[ (this.currentPiecePosition.pieceIndex + 1) % this.game.track.pieces.length ].switchTo;
      }
      this.pieceLength = this.game.track.pieceLength( this.game.track.pieces[ this.oldPiece ], this.currentPiecePosition.lane.startLaneIndex );
    }

    // Zurückgelegte Strecke und Geschwindigkeit berechnen
    var newRaceDistance = this.raceDistanceOffset + this.currentPiecePosition.inPieceDistance;
    this.speed = newRaceDistance - this.raceDistance;
    this.raceDistance = newRaceDistance;
    this.speedHistory[ this.game.currentTick ] = this.speed;
/*
    var newDriftEnergy = this.driftEnergy * 0.9 - this.driftAngle * 0.004492;
    var newDriftAngle = this.driftAngle + this.driftEnergy;
    var diffEnergy = this.currentAngle - newDriftAngle;
    var angle = this.game.track.pieces[ this.currentPiecePosition.pieceIndex ].angle; if( !angle ) angle = 0;
    var radius = this.game.track.pieces[ this.currentPiecePosition.pieceIndex ].radius; if( !radius ) radius = 0;
    var lane = this.currentPiecePosition.lane.startLaneIndex;
    if( angle < 0 ) radius = -(radius + this.game.track.lanes[ lane ].distanceFromCenter);
    if( angle > 0 ) radius = (radius - this.game.track.lanes[ lane ].distanceFromCenter);
    console.log( (new Number(radius)).toFixed(8) + '\t' + 
                 (new Number(this.speed)).toFixed(8) + '\t' + 
                 (new Number(diffEnergy)).toFixed(8) + '\t' + 
                 (new Number(this.currentAngle)).toFixed(8) );
    this.driftEnergy = newDriftEnergy + diffEnergy;
    this.driftAngle = this.currentAngle;
*/
    // Reibung und Leistung ermitteln
    if( this.game.currentTick == 3 ) {
      Speed1 = this.speedHistory[ this.game.currentTick - 2 ];
      Speed2 = this.speedHistory[ this.game.currentTick - 1 ];
      Speed3 = this.speedHistory[ this.game.currentTick - 0 ];
      this.friction = (Speed2 - Speed3) / (Speed1 - Speed2);
      this.power = Speed3 - Speed2 * this.friction;
      console.log( 'Reibung: ' + this.friction );
      console.log( 'Leistung: ' + this.power );
    }

    // Geplante Geschwindigkeit ermitteln und herbeiführen
    this.throttle = this.calcThrottle( );

    // Interessante Daten aufbewahren
    this.logData( );

    // Fahrspur wechseln oder beschleunigen
    if( switchTo ) {
      return { msgType: 'switchLane', data: switchTo };
    } else if( this.throttle > 1 ) {
      return { msgType: 'turbo', data: 'Pow pow pow pow pow, or your of personalized turbo message' };
    } else {
      return { msgType: 'throttle', data: this.throttle };
    }

  },

  //
  //  Todo: Das sollte dem Konzept nach woanders passieren
  //
  logData: function( ) {

    var track = this.game.track;
    var piece = this.getCurrentPieceInfo( );
    var radius = piece.radius;
    var angle = piece.angle;
    var lane = this.currentPiecePosition.lane.startLaneIndex;
    var pieceType = 'g';

    if( angle < 0 ) {
      pieceType = 'l';
      radius = -(radius + this.game.track.lanes[ lane ].distanceFromCenter);
    } else if( angle > 0 ) {
      pieceType = 'r';
      radius = (radius - this.game.track.lanes[ lane ].distanceFromCenter);
    } else {
      radius = 0;
    }

    if( piece[ 'switch' ] ) pieceType += 'x';

    this.loggedData.push(
      this.currentPiecePosition.lap + '/' + this.currentPiecePosition.pieceIndex + pieceType + '\t' +
      (new Number( this.game.currentTick )).toFixed( 0 ) + '\t' +
      (new Number( this.raceDistance )).toFixed( 8 ) + '\t' +
      (new Number( this.speed )).toFixed( 8 ) + '\t' +
      (new Number( this.currentAngle )).toFixed( 8 ) + '\t' +
      (new Number( radius )).toFixed( 8 ) + '\t' +
      lane + '\t' +
      (new Number( this.throttle )).toFixed( 8 )
    );

  },

  //
  //
  //
  receiveCrash: function( ) {

    console.log( 'Abflug: ' + this.currentAngle + '°' );

    this.isCrashed = true;

  },

  //
  //
  //
  receiveSpawn: function( ) {

    this.isCrashed = false;

  },

  //
  // returns: {"radius":50,"angle":45} or {"length":100}
  //
  getCurrentPieceInfo: function( ) {

    return this.game.track.getPiece( this.currentPiecePosition.pieceIndex );

  },

  //
  //
  //
  getNextPieceInfo: function( ) {

    return this.game.track.getPiece( this.currentPiecePosition.pieceIndex + 1 );

  },

  //
  //  Ermittelt die Spurwechsel zur kürzesten Strecke. Achtung: Dies ist
  //  nicht notwendigerweise die Ideallinie
  //
  laneSwitching: function( position, lane, distance ) {

    var switchLane;

    if( typeof position == 'undefined' ) position = 0;
    if( typeof lane == 'undefined' ) lane = 0;
    if( typeof distance == 'undefined' ) distance = 0;

    if( position == this.game.track.pieces.length ) {
      return distance;
    }

    distance += this.game.track.pieceLength( this.game.track.pieces[ position ], lane );

    if( this.game.track.pieces[ position ][ 'switch' ] ) {
      this.game.track.pieces[ position ].switchTo = 'Left';
      switchLane = lane - 1; if( switchLane < 0 ) switchLane = 0;
      var leftDistance = this.laneSwitching( position + 1, switchLane, distance );
      this.game.track.pieces[ position ].switchTo = 'Right';
      switchLane = lane + 1; if( switchLane >= this.game.track.lanes.length ) switchLane = this.game.track.lanes.length - 1;
      var rightDistance = this.laneSwitching( position + 1, switchLane, distance );
      this.game.track.pieces[ position ].switchTo = false;
      var falseDistance = this.laneSwitching( position + 1, lane, distance );

      if( falseDistance < leftDistance ) {
        if( falseDistance < rightDistance ) {
          this.game.track.pieces[ position ].switchTo = false;
        } else {
          this.game.track.pieces[ position ].switchTo = 'Right';
        }
      } else {
        if( leftDistance < rightDistance ) {
          this.game.track.pieces[ position ].switchTo = 'Left';
        } else {
          this.game.track.pieces[ position ].switchTo = 'Right';
        }
      }
    } else {
      distance = this.laneSwitching( position + 1, lane, distance );
    }

    return distance;

  },

  //
  //  Ermittelt die momentan angemessene Beschleunigung auf Basis der
  //  maximalen - derzeit von Hand festgelegten - Kurzvengeschwindigkeit des
  //  Streckenverlaufs
  //
  //  Todo: maximale Kurvengechwindigkeit nicht fest verdrahten sondern auf
  //        Basis der bisherigen Erfahrungswerte auf dieser Strecke berechnen
  //  Todo: In der letzten Runde (Achtung: Qualifying ist zeitbasiert) darf
  //        der Turbo auch über die Zielline verwendet werden.
  //  Todo: Turbo fehlt noch
  //
  calcThrottle: function( ) {

    var speed = this.speed;
    var maxThrottle = 1;
    var pieceIndex = this.currentPiecePosition.pieceIndex;
    var inPieceDistance = this.currentPiecePosition.inPieceDistance;
    var lane = this.currentPiecePosition.lane.startLaneIndex;

    if( this.currentAngle > 59 ) {
      return 0;
    }

    while( speed > 3.5 ) {
      var speedLimit = 100;
      if( this.game.track.pieces[ pieceIndex ].radius ) {
        var radius = Math.abs( this.game.track.pieces[ pieceIndex ].radius );
        if( this.game.track.pieces[ pieceIndex ].angle < 0 ) {
          radius = (radius + this.game.track.lanes[ lane ].distanceFromCenter);
        } else {
          radius = (radius - this.game.track.lanes[ lane ].distanceFromCenter);
        }
        speedLimit = 2;
        if( radius >= 40 ) speedLimit = 4.67; // 4.68 ist langsamer auf germany
        if( radius >= 60 ) speedLimit = 5.59;
        if( radius >= 90 ) speedLimit = 6.46; // < 6.47
        if( radius >= 110 ) speedLimit = 6.91; // 6.92 ist langsamer auf keimola
        if( radius >= 180 ) speedLimit = 9.20; // < 9.21
      }
      if( speed > speedLimit ) { // Bremsen?
        var throttle = (speedLimit - speed * this.friction) / this.power;
        if( throttle < 0 ) throttle = 0;
        if( maxThrottle > throttle ) maxThrottle = throttle;
      } else if( speed > this.friction * speedLimit ) {
        if( maxThrottle > speedLimit / 10.0 ) maxThrottle = speedLimit / 10.0;
      }
      speed *= this.friction;
      inPieceDistance += speed;
      var inPieceLength = this.game.track.pieceLength( this.game.track.pieces[ pieceIndex ], lane );
      if( inPieceDistance >= inPieceLength ) {
        inPieceDistance -= inPieceLength;
        pieceIndex = (pieceIndex + 1) % this.game.track.pieces.length;
        if( (this.game.track.pieces[ pieceIndex ].switchTo == 'Left') && (lane > 0) ) {
          lane--;
        } else if( (this.game.track.pieces[ pieceIndex ].switchTo == 'Right') && (lane < this.game.track.lanes.length - 1) ) {
          lane++;
        }
      }
    }

    if( this.hasTurbo && (maxThrottle >= 1) ) {
      var distance = 0, pieceIndex = this.currentPiecePosition.pieceIndex;
      while( !this.game.track.pieces[ pieceIndex ].radius ) {
        distance += this.game.track.pieces[ pieceIndex ].length;
        pieceIndex++;
        if( pieceIndex == this.game.track.pieces.length ) {
          pieceIndex -= this.game.track.pieces.length;
        }
      }
      if( distance >= 400 ) {
        return 3;
      }
    }

    return maxThrottle;

  },

  //
  //
  //
  receiveTurbo: function( req ) {

    this.hasTurbo = true;

  },

  //
  //
  //
  receiveTurboStart: function( req ) {

    this.hasTurbo = false;

  },

  //
  //  Todo: Inhalt?
  //
  receiveTurboEnd: function( req ) {

  }

};

exports.car = car;
