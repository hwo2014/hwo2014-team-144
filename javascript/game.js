var cars = require( './car' );
var tracks = require( './track' );

var game = {
  com: null,

  name: 'ryan',
  key: 'c7dTGj3xYFKYbg',
  trackName: '',
  carCount: 1,
  gameId: '',
  isJoined: false,
  isInit: false,
  isStarted: false,

  myCar: null,
  cars: [],

  track: null,

  raceSession: null,

  currentTick: -1,

  //
  //
  //
  init: function( req ) {

    // send a join
    return this.sendJoin( );

  },

  //
  //
  //
  sendJoin: function( req ) {

    if( this.trackName ) {
      return { msgType: 'joinRace', data: { botId: { name: this.name, key: this.key }, trackName: this.trackName, carCount: this.carCount } };
    } else {
      return { msgType: 'join', data: { name: this.name, key: this.key } };
    }

  },

  //
  //
  //
  receiveJoin: function( req ) {

    if( req.msgType == 'join' && req.data.name == this.name && req.data.key == this.key ) {
      //console.log( 'LOG: Received Join success' );
      this.isJoined = true;
    } else if( req.msgType == 'createRace' && req.data.botId.name == this.name && req.data.botId.key == this.key ) {
      //console.log( 'LOG: Received create race success' );
      this.isJoined = true;
    }

  },

  //
  //
  //
  initCar: function( data ) {

    var car = Object.create( cars.car );
    car.init( data );
    // also save it to cars and to my car
    this.cars.push( car );
    return car;

  },

  //
  //
  //
  receiveCarInfo: function( req ) {

    //console.log( 'LOG: Received Car info ' );
    var car = this.initCar( req.data );
    this.myCar = car;

  },

  //
  //
  //
  receiveCarPositions: function( req ) {

    //console.log( 'LOG: Car positions received' );
    for( var i = 0; i < req.data.length; i++ ) {
      var data = req.data[ i ];
      var car = this.getCarByInfo( data.id );
      if( !car ) continue; // TODO: ALSO ERROR
      car.updateCarPosition( data );
    }

    // also we want to send our car information IF our game is started
    if( this.isStarted ) {
      this.com.send( this.myCar.sendMessage( ) );
      // also update current gameTick
      this.currentTick = req.gameTick;
    }

  },

  //
  //
  //
  receiveInit: function( req ) {

    //console.log( 'LOG: Game is beinig initialized' );
    // initialize the track
    this.initTrack( req.data.race.track );
    this.initCars( req.data.race.cars );
    this.raceSession = req.data.race.raceSession;
    this.isInit = true;

  },

  //
  //
  //
  receiveGameStart: function( req ) {

    //console.log( 'LOG: Game is being started' );
    this.currentTick = req.gameTick;
    this.isStarted = true;
    if( req.data ) console.log( 'WARN: Data is NOT null, that was unexpected '+JSON.stringify( req.data ) );
    // also now we want to send our first initial car info
    this.com.send( this.myCar.sendMessage( ) );

  },

  //
  //
  //
  receiveCrash: function( req ) {

    //console.log( 'LOG: Received crash' );
    // {"msgType":"crash","data":{"name":"ryan","color":"red"},"gameId":"4d2e27b2-b7cd-4701-a898-9564048ab41e","gameTick":120}
    var car = this.getCarByInfo( req.data );
    car.receiveCrash( req );

  },

  //
  //
  //
  receiveSpawn: function( req ) {

    //console.log( 'LOG: Received spawn' );
    // {"msgType":"spawn","data":{"name":"ryan","color":"red"},"gameId":"5117a1b8-3f75-4ed9-81e6-2c6973971205","gameTick":520}
    var car = this.getCarByInfo( req.data );
    car.receiveSpawn( req );

  },

  //
  //
  //
  receiveGameEnd: function( req ) {

    // TODO
    // {"msgType":"gameEnd","data":{"results":[{"car":{"name":"ryan","color":"red"},"result":{"laps":3,"ticks":1664,"millis":27734}}],"bestLaps":[{"car":{"name":"ryan","color":"red"},"result":{"lap":1,"ticks":538,"millis":8966}}]},"gameId":"d8790092-7be4-441e-a1f9-82dae7f85f16"}
    //console.log( 'INFO: Game End' );

    console.log( '' );
    for( var i = 0; i < this.myCar.loggedData.length; i++ ) {
      console.log( this.myCar.loggedData[ i ] );
    }

  },

  //
  //
  //
  receiveTournamentEnd: function( req ) {

    // TODO
    // {"msgType":"tournamentEnd","data":null,"gameId":"772e6fc9-3429-4bbb-8ce0-78509bd75c5a"}
    //console.log( 'INFO: Tournament ended' );

  },

  //
  //
  //
  receiveLapInfo: function( req ) {

    // TODO
    // {"msgType":"lapFinished","data":{"car":{"name":"ryan","color":"red"},"lapTime":{"lap":0,"ticks":587,"millis":9784},"raceTime":{"laps":1,"ticks":588,"millis":9800},"ranking":{"overall":1,"fastestLap":1}},"gameId":"de0d9ff0-a743-460c-8c32-dd67a19fb2df","gameTick":588}
    console.log( 'Rundenzeit ' + req.data.car.name + ': ' + req.data.lapTime.millis );

  },

  //
  //
  //
  receiveFinish: function( req ) {

    // TODO:
    // {"msgType":"finish","data":{"name":"ryan","color":"red"},"gameId":"ed57776a-db44-4faa-be43-ae6a3461eb44"}
    //console.log( 'INFO: Finished Game' );

  },

  //
  //
  //
  receiveDnf: function( req ) {

    //console.log( 'INFO: Got disqualified with reason: '+req.data.reason );

  },

  //
  //
  //
  receiveTurboAvailable: function( req ) {

    this.myCar.receiveTurbo( req );

  },

  //
  //
  //
  receiveTurboStart: function( req ) {

    this.myCar.receiveTurboStart( req );

  },

  //
  //
  //
  receiveTurboEnd: function( req ) {

    this.myCar.receiveTurboEnd( req );

  },

  //
  //
  //
  initTrack: function( data ) {

    this.track = Object.create( tracks.track );
    this.track.init( data );
    for( var i = 0; i < this.cars.length; i++ ) {
      this.cars[ i ].game = this;
      this.cars[ i ].laneSwitching( );
    }

  },

  //
  //
  //
  initCars: function( data ) {

    for( var i = 0; i < data.length; i++ ) {
      var carData = data[ i ];
      var car = this.getCarByInfo( carData.id );
      // only add other cars, as we already know our own
      if( !car ) {
        car = this.initCar( carData.id );
      }
      car.updateCarDimensions( carData.dimensions );
      // also give any car a pointer to the current game object
      car.game = this;
    }

  },

  //
  //
  //
  getCarByInfo: function( data ) {

    for( var i = 0; i < this.cars.length; i++ ) {
      var car = this.cars[ i ];
      if( data.color == car.color ) return car;
    }

    return null;

  }

};

exports.game = game;
